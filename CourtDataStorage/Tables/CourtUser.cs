﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtDataStorage.Tables
{
    public class CourtUser
    {
        public int Id { get; set; }
        public String UserName { get; set; }
        public String Password { get; set; }
        public String Email { get; set; }
        public List<CourtCase> Cases { get; set; }
        public AccessLevel Level { get; set; }
    }

    public enum AccessLevel
    {
        User,
        Secretary,
        Administrator
    }
}
