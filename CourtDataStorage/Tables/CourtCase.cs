﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtDataStorage.Tables
{
    public class CourtCase
    {
        public int Id { get; set; }
        public virtual CourtUser CourtUser { get; set; }
        public String CaseNumber { get; set; }
        public virtual Participant Claimant { get; set; }
        public virtual List<Participant> Respondents { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public DateTime? GatheringDate { get; set; }
        public Decision? Decision { get; set; }
        public DateTime? VerdictDate { get; set; }
        public String Commentary { get; set; }
        public String AttachmentName { get; set; }
        public byte[] Attachment { get; set; }
        public virtual Claim Claim { get; set; }
    }

    public enum Scenario
    {
        NotSet,
        Standard,
        NotVerbal
    }

    public enum Decision
    {
        Submitted = 0,
        Accepted = 1,
        Refused = 2,
        Closed = 3
    }
}
