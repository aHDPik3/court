﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtDataStorage.Tables
{
    public class Participant
    {
        public int Id { get; set; }
        public virtual Physical Physical { get; set; }
        public virtual Juridical Juridical { get; set; }
        public virtual Individual Individual { get; set; }
    }
}
