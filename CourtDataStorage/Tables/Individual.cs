﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtDataStorage.Tables
{
    public class Individual
    {
        public int Id { get; set; }
        public String INN { get; set; }
        public String LastName { get; set; }
        public String FirstName { get; set; }
        public String Patronymic { get; set; }
        public DateTime? BirthDate { get; set; }
        public String BirthPlace { get; set; }
        public String OGRNIP { get; set; }
        public virtual Country Citizenship { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public String RegistrationPlace { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public virtual Address Address { get; set; }
    }
}
