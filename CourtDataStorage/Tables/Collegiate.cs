﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CourtDataStorage.Tables
{
    public class Collegiate
    {
        public int Id { get; set; }
        [Display(Name = "Название")]
        public String Name { get; set; }
        public bool Deleted { get; set; }
    }
}
