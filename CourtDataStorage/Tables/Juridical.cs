﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtDataStorage.Tables
{
    public class Juridical
    {
        public int Id { get; set; }
        public String INN { get; set; }
        public String FullName { get; set; }
        public String ShortName { get; set; }
        public String SubDivision { get; set; }
        public String OGRN { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public String RegistrationPlace { get; set; }
        public String RegistrationStatus { get; set; }
        public virtual Address Address { get; set; }
        public String ContactFullName { get; set; }
        public String ContactMobilePhoneNumber { get; set; }
        public String ContactWorkPhoneNumber { get; set; }
        public String ContactEmail { get; set; }
    }
}
