﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtDataStorage.Tables
{
    public class Physical
    {
        public int Id { get; set; }
        public String LastName { get; set; }
        public String FirstName { get; set; }
        public String Patronymic { get; set; }
        public DateTime? BirthDate { get; set; }
        public String BirthPlace { get; set; }
        public virtual Country Citizenship { get; set; }
        public virtual Address Address { get; set; }
    }
}
