﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtDataStorage.Tables
{
    public class Address
    {
        public int Id { get; set; }
        public String City { get; set; }
        public virtual Country Country { get; set; }
        public String Street { get; set; }
        public String House { get; set; }
        public String Index { get; set; }
    }
}
