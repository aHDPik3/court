﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtDataStorage.Tables
{
    public class Claim
    {
        public int Id { get; set; }
        public virtual Collegiate Collegiate { get; set; }
        public virtual ClaimReason Reason { get; set; }
        public bool IsCommercial { get; set; }
        public bool NotVerbal { get; set; }
        public bool IsCollegiate { get; set; }

    }


}
