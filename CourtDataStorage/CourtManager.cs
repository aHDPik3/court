﻿using CourtDataStorage.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtDataStorage
{
    public class CourtManager
    {
        private CourtContext context;

        public CourtManager(bool doReset)
        {
            context = new CourtContext();
            if (doReset)
            {
                context.Database.Delete();


            }
            if (context.CourtUsers.Count() == 0)
            {
                CourtUser user = new CourtUser()
                {
                    Email = "admin@admin.com",
                    UserName = "admin",
                    Password = "admin",
                    Level = AccessLevel.Administrator
                };
                context.CourtUsers.Add(user);
                user = new CourtUser()
                {
                    Email = "secretary@secretary.com",
                    UserName = "secretary",
                    Password = "secretary",
                    Level = AccessLevel.Secretary
                };
                context.CourtUsers.Add(user);
            }
            if (context.Collegiates.Count() == 0)
            {
                context.Collegiates.Add(new Collegiate() { Name = "Коллегия 1" });
                context.Collegiates.Add(new Collegiate() { Name = "Коллегия 2" });
                context.Collegiates.Add(new Collegiate() { Name = "Коллегия 3" });
                context.Collegiates.Add(new Collegiate() { Name = "Коллегия 4" });
                context.SaveChanges();
            }
            if (context.ClaimReasons.Count() == 0)
            {
                context.ClaimReasons.Add(new ClaimReason() { Name = "Задолженность по кредитному договору" });
                context.ClaimReasons.Add(new ClaimReason() { Name = "Задолженность по договору займа" });
                context.ClaimReasons.Add(new ClaimReason() { Name = "Задолженность по договору лизинга" });
                context.ClaimReasons.Add(new ClaimReason() { Name = "Задолженность по договору ипотеки" });
                context.SaveChanges();
            }
            if (context.Countries.Count() == 0)
            {
                context.Countries.Add(new Country() { Name = "Россия" });
                context.Countries.Add(new Country() { Name = "Китай" });
            }
        }

        public List<Collegiate> GetAllCollegiates()
        {
            return context.Collegiates.Where(c => !c.Deleted).ToList();
        }

        public List<ClaimReason> GetAllClaimReasons()
        {
            return context.ClaimReasons.Where(c => !c.Deleted).ToList();
        }

        public List<CourtUser> GetAllUsers()
        {
            return context.CourtUsers.ToList();
        }

        public List<CourtCase> GetAllCases()
        {
            return context.CourtCases.ToList();
        }

        public ClaimReason GetReason(int id)
        {
            return context.ClaimReasons.Find(id);
        }

        public Country GetCountry(int id)
        {
            return context.Countries.Find(id);
        }

        public List<Country> GettAllAvailableCountries()
        {
            return context.Countries.Where(c => !c.Deleted).ToList();
        }

        public Collegiate GetCollegiate(int id)
        {
            return context.Collegiates.Find(id);
        }

        public CourtUser GetUser(int id)
        {
            return context.CourtUsers.Find(id);
        }

        public CourtUser FindUserByName(String name)
        {
            CourtUser courtUser = context.CourtUsers.FirstOrDefault(u => u.UserName == name);
            context.Entry<CourtUser>(courtUser).Collection(cu => cu.Cases).Load();
            return courtUser;
        }

        public CourtUser FindUserByNameAndPassword(String name, String password)
        {
            CourtUser courtUser = context.CourtUsers.FirstOrDefault(u => u.UserName == name && u.Password == password);
            return courtUser;
        }

        public bool HasUser(String name)
        {
            return context.CourtUsers.Any(u => u.UserName == name);
        }

        public void AddUser(String userName, String password, String email)
        {
            CourtUser user = new CourtUser()
            {
                UserName = userName,
                Password = password,
                Email = email,
                Level = AccessLevel.User
            };
            context.CourtUsers.Add(user);
            context.SaveChanges();
        }

        public void UpdateParticipant(Participant participant)
        {
            
            if (participant.Individual != null && participant.Individual.Address!=null && participant.Individual.Address.Country != null)
            {
                participant.Individual.Address.Country = GetCountry(participant.Individual.Address.Country.Id);
            }
            if (participant.Individual != null && participant.Individual.Address != null && participant.Individual.Citizenship != null)
            {
                participant.Individual.Citizenship = GetCountry(participant.Individual.Citizenship.Id);
            }
            if (participant.Physical != null && participant.Physical.Address != null && participant.Physical.Address.Country != null)
            {
                participant.Physical.Address.Country = GetCountry(participant.Physical.Address.Country.Id);
            }
            if (participant.Physical != null && participant.Physical.Address != null && participant.Physical.Citizenship != null)
            {
                participant.Physical.Citizenship = GetCountry(participant.Physical.Citizenship.Id);
            }
            if (participant.Juridical != null && participant.Juridical.Address != null && participant.Juridical.Address.Country != null)
            {
                participant.Juridical.Address.Country = GetCountry(participant.Juridical.Address.Country.Id);
            }
        }

        public void RefreshRelatedEntities(CourtCase courtCase)
        {
            UpdateParticipant(courtCase.Claimant);
            foreach (Participant respondent in courtCase.Respondents)
            {
                UpdateParticipant(respondent);
            }
            if (courtCase.Claim.Reason != null)
            {
                courtCase.Claim.Reason = GetReason(courtCase.Claim.Reason.Id);
            }
            if (courtCase.Claim.Collegiate != null)
            {
                courtCase.Claim.Collegiate = GetCollegiate(courtCase.Claim.Collegiate.Id);
            }
        }

        public CourtCase AddCourtCase(CourtCase courtCase)
        {
            RefreshRelatedEntities(courtCase);
            context.CourtCases.Add(courtCase);
            context.SaveChanges();
            CourtCase cCase = context.Entry(courtCase).Entity;
            cCase.CaseNumber = cCase.Claim.Collegiate.Name + "-" + cCase.Id;
            cCase.Decision = 0;
            /*foreach(Respondent respondent in courtCase.Respondents)
            {
                respondent.CourtCase = cCase;
                context.Respondents.Add(respondent);
            }*/
            context.SaveChanges();
            return cCase;
        }

        public CourtCase GetCase(int id)
        {
            return context.CourtCases.Find(id);
        }

        public void DeleteAttachment(int id)
        {
            CourtCase cCase = GetCase(id);
            cCase.Attachment = null;
            cCase.AttachmentName = null;
            context.SaveChanges();
        }

        public void UpdateCase(int caseId, Decision decision, String comment, DateTime? date, byte[] attachment, String fileName)
        {

            CourtCase courtCase = GetCase(caseId);
            if (attachment != null)
            {
                courtCase.Attachment = attachment;
                courtCase.AttachmentName = fileName;
            }
            courtCase.Decision = decision;
            courtCase.Commentary = comment;

            courtCase.GatheringDate = date;
            courtCase.VerdictDate = decision == Decision.Closed || decision == Decision.Refused ? DateTime.Now : (DateTime?)null;
            context.SaveChanges();
        }

        public void UpdateUserLevel(int id, AccessLevel level)
        {
            GetUser(id).Level = level;
            context.SaveChanges();
        }

        public void DeleteCountry(int id)
        {
            context.Countries.Find(id).Deleted = true;
            context.SaveChanges();
        }

        public void EditCountry(int id, String name)
        {
            if (id < 0)
            {
                Country country = new Country()
                {
                    Deleted = false,
                    Name = name
                };
                context.Countries.Add(country);
            }
            else
            {
                Country country = GetCountry(id);
                country.Name = name;
            }
            context.SaveChanges();
        }

        public void DeleteCollegiate(int id)
        {
            context.Collegiates.Find(id).Deleted = true;
            context.SaveChanges();
        }

        public void EditCollegiate(int id, String name)
        {
            if (id < 0)
            {
                Collegiate collegiate = new Collegiate()
                {
                    Deleted = false,
                    Name = name
                };
                context.Collegiates.Add(collegiate);
            }
            else
            {
                Collegiate collegiate = GetCollegiate(id);
                collegiate.Name = name;
            }
            context.SaveChanges();
        }

        public void DeleteReason(int id)
        {
            context.ClaimReasons.Find(id).Deleted = true;
            context.SaveChanges();
        }

        public void EditReason(int id, String name)
        {
            if (id < 0)
            {
                ClaimReason reason = new ClaimReason()
                {
                    Deleted = false,
                    Name = name
                };
                context.ClaimReasons.Add(reason);
            }
            else
            {
                ClaimReason reason = GetReason(id);
                reason.Name = name;
            }
            context.SaveChanges();
        }
    }
}
