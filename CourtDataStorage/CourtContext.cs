﻿using CourtDataStorage.Tables;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtDataStorage
{
    public class CourtContext:DbContext
    {
        public DbSet<Claim> Claims { get; set; }
        public DbSet<Collegiate> Collegiates { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<CourtCase> CourtCases { get; set; }
        public DbSet<CourtUser> CourtUsers { get; set; }
        public DbSet<Individual> Individuals { get; set; }
        public DbSet<Juridical> Juridicals { get; set; }
        public DbSet<Physical> Physicals { get; set; }
        public DbSet<Participant> Participants { get; set; }
        public DbSet<ClaimReason> ClaimReasons { get; set; }
    }
}
