﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Court.Models
{
    public class ForwardModel
    {
        public int CaseId { get; set; }
        [Display(Name ="Выберите действие")]
        public int ActionId { get; set; }
        [Display(Name ="Комментарии")]
        public String Comments { get; set; }
        [Display(Name ="Дата заседания")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }
        [Display(Name ="Приложение")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase Attachment { get; set; }
        public String AttachmentName { get; set; }
    }
}