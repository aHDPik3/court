﻿using CourtDataStorage.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Court.Models
{
    public class JuridicialModel
    {
        [Required]
        [Display(Name ="ИНН*")]
        [RegularExpression("\\d{12}", ErrorMessage = "ИНН должен состоять из 12 цифр")]
        public String INN { get; set; }
        [Required]
        [Display(Name = "Полное наименование*")]
        public String FullName { get; set; }
        [Required]
        [Display(Name = "Сокращённое наименование*")]
        public String ShortName { get; set; }
        [Display(Name = "Филиал/подразделение")]
        public String SubDivision { get; set; }
        [Required]
        [Display(Name = "ОГРН*")]
        [RegularExpression("\\d{13}", ErrorMessage = "ОГРН должен состоять из 13 цифр")]
        public String OGRN { get; set; }
        [Display(Name = "Дата гос. регистрации")]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1/1/1900", "1/1/3000", ErrorMessage = "Дата не верна")]
        public DateTime? RegistrationDate { get; set; }
        [Display(Name = "Место гос. регистрации")]
        public String RegistrationPlace { get; set; }
        [Display(Name = "Состояние юридичнского лица")]
        public String RegistrationStatus { get; set; }
        [Display(Name ="Город")]
        public String City { get; set; }
        [Display(Name = "Страна")]
        public int CountryId { get; set; }
        [Display(Name = "Улица")]
        public String Street { get; set; }
        [Display(Name = "Дом")]
        public String House { get; set; }
        [Display(Name = "Индекс")]
        public String Index { get; set; }
        [Display(Name = "ФИО")]
        public String ContactFullName { get; set; }
        [Display(Name = "Мобильный телефон")]
        public String ContactMobilePhoneNumber { get; set; }
        [Display(Name = "Рабочий телефон")]
        [DataType(DataType.PhoneNumber)]
        public String ContactWorkPhoneNumber { get; set; }
        [Display(Name = "E-mail")]
        [DataType(DataType.EmailAddress)]
        public String ContactEmail { get; set; }

        public Juridical ToJuridical(CourtDataStorage.CourtManager manager)
        {
            Address address = new Address()
            {
                City = City,
                Country = manager.GetCountry(CountryId),
                House = House,
                Index = Index,
                Street = Street
            };
            return new Juridical()
            {
                Address = address,
                ContactEmail = ContactEmail,
                ContactFullName = ContactFullName,
                ContactMobilePhoneNumber = ContactMobilePhoneNumber,
                ContactWorkPhoneNumber = ContactWorkPhoneNumber,
                FullName = FullName,
                INN = INN,
                OGRN = OGRN,
                RegistrationDate = RegistrationDate,
                RegistrationPlace = RegistrationPlace,
                RegistrationStatus = RegistrationStatus,
                ShortName = ShortName,
                SubDivision = SubDivision
            };
        }
    }
}