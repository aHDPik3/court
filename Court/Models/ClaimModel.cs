﻿using CourtDataStorage;
using CourtDataStorage.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Court.Models
{
    public class ClaimModel
    {
        [Required]
        [Display(Name = "Территориальная коллегия")]
        public int CollegiateNumber { get; set; }
        [Required]
        [Display(Name = "Суть спора")]
        public int ReasonNumber { get; set; }
        [Display(Name = "Спор связан с предпринимательской деятельностью")]
        public bool IsCommercial { get; set; }
        [Display(Name = "Без устных слушаний?")]
        public bool NotVerbal { get; set; }
        [Display(Name = "Коллегиальный состав")]
        public bool IsCollegiate { get; set; }

        public Claim ToClaim(CourtManager manager)
        {
            return new Claim()
            {
                IsCollegiate = IsCollegiate,
                NotVerbal = NotVerbal,
                IsCommercial = IsCommercial,
                Collegiate = manager.GetCollegiate(CollegiateNumber),
                Reason = manager.GetReason(ReasonNumber)
            };
        }
    }
}
