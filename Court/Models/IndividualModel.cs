﻿using CourtDataStorage.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Court.Models
{
    public class IndividualModel
    {
        [Required]
        [Display(Name ="ИНН*")]
        [RegularExpression("\\d{12}",ErrorMessage ="ИНН должен состоять из 12 цифр")]
        public String INN { get; set; }
        [Required]
        [Display(Name ="Фамилия*")]
        public String Sirname { get; set; }
        [Required]
        [Display(Name = "Имя*")]
        public String FirstName { get; set; }
        [Display(Name = "Отчество")]
        public String Patronymic { get; set; }
        [Required]
        [Display(Name = "Дата рождения*")]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1/1/1900", "1/1/3000", ErrorMessage = "Дата не верна")]
        public DateTime BirthDate { get; set; }
        [Required]
        [Display(Name = "Место рождения*")]
        public String BirthPlace { get; set; }
        [Required]
        [Display(Name = "ОГРНИП*")]
        [RegularExpression("\\d{15}", ErrorMessage = "ОГРНИП должен состоять из 15 цифр")]
        public String OGRNIP { get; set; }
        [Required]
        [Display(Name = "Гражданство*")]
        public int Citizenship { get; set; }
        [Display(Name = "Дата регистрации")]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1/1/1900", "1/1/3000", ErrorMessage = "Дата не верна")]
        public DateTime? RegistrationDate { get; set; }
        [Display(Name = "Место регистрации")]
        public String RegistrationPlace { get; set; }
        [Display(Name = "Дата прекращения деятельности")]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1/1/1900", "1/1/3000", ErrorMessage = "Дата не верна")]
        public DateTime? ExpirationDate { get; set; }
        [Display(Name = "Город")]
        public String City { get; set; }
        [Display(Name = "Страна")]
        public int CountryId { get; set; }
        [Display(Name = "Улица")]
        public String Street { get; set; }
        [Display(Name = "Дом")]
        public String House { get; set; }
        [Display(Name = "Индекс")]
        public String Index { get; set; }

        public Individual ToIndividual(CourtDataStorage.CourtManager manager)
        {
            Address address = new Address()
            {
                City = City,
                Country = manager.GetCountry(CountryId),
                House = House,
                Index = Index,
                Street = Street
            };
            return new Individual
            {
                Address = address,
                BirthDate = BirthDate,
                BirthPlace = BirthPlace,
                Citizenship= manager.GetCountry(Citizenship),
                ExpirationDate = ExpirationDate,
                FirstName = FirstName,
                INN = INN,
                OGRNIP = OGRNIP,
                Patronymic = Patronymic,
                RegistrationDate = RegistrationDate,
                RegistrationPlace = RegistrationPlace,
                LastName = Sirname
            };
        }
    }
}