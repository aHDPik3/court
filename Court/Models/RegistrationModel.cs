﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Court.Models
{
    public class RegistrationModel
    {
        [Required]
        [StringLength(20,ErrorMessage ="Имя пользователя должно содержать от 4 до 20 символов",MinimumLength =4)]
        [RegularExpression("^[a-zA-Z0-9_]*$",ErrorMessage ="Имя пользователя может состоять только из латинских символов и цифр")]
        [Display(Name ="Имя пользователя*")]
        public String UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [RegularExpression("^[a-zA-Z\\d]{8,}$",ErrorMessage = "Пароль должен содержать не менее 8 символов и состоять только из латинских символов и цифр")]
        [Display(Name = "Пароль*")]
        public String Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage ="Пароли не совпадают")]
        [Display(Name = "Подтвердить пароль*")]
        public String Confirm { get; set; }
        [Required]
        [DataType(DataType.EmailAddress,ErrorMessage ="Почта имеет неверный формат")]
        [Display(Name = "E-mail*")]
        public String Email { get; set; }
    }
}