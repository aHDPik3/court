﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Court.Models
{
    public class LoginModel
    {
        [Required]
        [Display(Name ="Имя пользователя")]
        public String UserName { get; set; }
        [Required]
        [Display(Name ="Пароль")]
        [DataType(DataType.Password)]
        public String Password { get; set; }
    }
}