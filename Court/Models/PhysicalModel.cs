﻿using CourtDataStorage.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Court.Models
{
    public class PhysicalModel
    {
        [Required]
        [Display(Name = "Фамилия*")]
        public String Sirname { get; set; }
        [Required]
        [Display(Name = "Имя*")]
        public String FirstName { get; set; }
        [Display(Name = "Отчество")]
        public String Patronymic { get; set; }
        [Required]
        [Display(Name = "Дата рождения*")]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1/1/1900", "1/1/3000", ErrorMessage = "Дата не верна")]
        public DateTime BirthDate { get; set; }
        [Required]
        [Display(Name = "Место рождения*")]
        public String BirthPlace { get; set; }
        [Required]
        [Display(Name = "Гражданство*")]
        public int Citizenship { get; set; }
        [Display(Name = "Город")]
        public String City { get; set; }
        [Display(Name = "Страна")]
        public int CountryId { get; set; }
        [Display(Name = "Улица")]
        public String Street { get; set; }
        [Display(Name = "Дом")]
        public String House { get; set; }
        [Display(Name = "Индекс")]
        public String Index { get; set; }

        public Physical ToPhysical(CourtDataStorage.CourtManager manager)
        {
            Country country = manager.GetCountry(CountryId);
            Address address = null;
            if (City != null || country != null || House != null || Index != null || Street != null)
            {
                address = new Address()
                {
                    City = City,
                    Country = country,
                    House = House,
                    Index = Index,
                    Street = Street
                };
            }
            return new Physical()
            {
                Address = address,
                BirthDate = BirthDate,
                BirthPlace = BirthPlace,
                FirstName = FirstName,
                Patronymic = Patronymic,
                LastName = Sirname,
                Citizenship = manager.GetCountry(Citizenship)
            };
        }

    }
}