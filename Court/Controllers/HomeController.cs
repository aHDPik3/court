﻿using CourtDataStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Court.Controllers
{
    public class HomeController : Controller
    {
        CourtManager manager = new CourtManager(false);
        // GET: Home
        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult Delete()
        {
            manager = new CourtManager(true);
            return Redirect("/Home/Index");
        }
    }
}