﻿using Court.Models;
using CourtDataStorage;
using CourtDataStorage.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Court.Controllers
{
    public class AccountController : Controller
    {
        CourtManager manager = new CourtManager(false);
        CourtUser currentUser = null;

        CourtUser CurrentUser
        {
            get
            {
                //currentUser = null;
                if (currentUser == null)
                {
                    currentUser = manager.FindUserByName(User.Identity.Name);
                }
                return currentUser;
            }
        }

        [Authorize]
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                CourtUser user = manager.FindUserByNameAndPassword(model.UserName, model.Password);
                if (user == null)
                {
                    ModelState.AddModelError("", "Неверный логин/пароль");
                    return View(model);
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    Session["isAdmin"] = user.Level == AccessLevel.Administrator;
                    return Redirect("/Home/Index");
                }
            }
        }


        public ActionResult Registration()
        {
            return View();
        }
        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session["isAdmin"] = false;
            return Redirect("/Home/Index");
        }
        [Authorize]
        public ActionResult UserList()
        {
            if(CurrentUser.Level == AccessLevel.Administrator)
            {
                return View(manager.GetAllUsers());
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [HttpPost]
        public ActionResult Registration(RegistrationModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                if (manager.HasUser(model.UserName))
                {
                    ModelState.AddModelError("UserName", "Имя пользователя уже занято");
                    return View(model);
                }
                else
                {
                    manager.AddUser(model.UserName, model.Password, model.Email);
                    return Redirect("/Home/Index");
                }
            }
        }

        public ActionResult UpdateStatus (int id, AccessLevel level)
        {
            if (CurrentUser.Id != id)
            {
                manager.UpdateUserLevel(id, level);
            }
            return Redirect("/Account/UserList");
            
        }

    }
}