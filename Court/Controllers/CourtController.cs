﻿using Court.Models;
using CourtDataStorage;
using CourtDataStorage.Tables;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Court.Controllers
{
    public class CourtController : Controller
    {
        CourtManager manager = new CourtManager(false);

        CourtUser currentUser = null;

        CourtUser CurrentUser
        {
            get
            {
                //currentUser = null;
                if (currentUser == null)
                {
                    currentUser = manager.FindUserByName(User.Identity.Name);
                }
                return currentUser;
            }
        }

        // GET: Court
        [Authorize]
        public ActionResult Index()
        {
            if (CurrentUser.Level == AccessLevel.User)
            {
                if (CurrentUser.Cases != null)
                    return View(CurrentUser.Cases.ToList());
                else
                    return View(new List<CourtCase>());
            }
            else
            {
                return View("Secretary", manager.GetAllCases());
            }
        }
        [Authorize]
        public ActionResult NewCase()
        {
            if (Session["court-case"] == null)
            {
                Session["court-case"] = new CourtCase();
            }
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddIndividual(IndividualModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("NewCase", model);
            }
            else
            {
                CourtCase courtCase = new CourtCase();
                courtCase.Claimant = new Participant()
                {
                    Individual = model.ToIndividual(manager)
                };
                Session["court-case"] = courtCase;
            }
            return Redirect("/Court/NewRespondent");
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddJuridicial(JuridicialModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("NewCase", model);
            }
            else
            {
                CourtCase courtCase = new CourtCase();
                courtCase.Claimant = new Participant()
                {
                    Juridical = model.ToJuridical(manager)
                };
                Session["court-case"] = courtCase;
            }
            return Redirect("/Court/NewRespondent");
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddPhysical(PhysicalModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("NewCase", model);
            }
            else
            {
                CourtCase courtCase = new CourtCase();
                courtCase.Claimant = new Participant()
                {
                    Physical = model.ToPhysical(manager)
                };
                Session["court-case"] = courtCase;
            }
            return Redirect("/Court/NewRespondent");
        }

        [Authorize]
        public ActionResult NewRespondent()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddIndividualRespondent(IndividualModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("NewRespondent", model);
            }
            else
            {

                CourtCase courtCase = Session["court-case"] as CourtCase;
                if (courtCase.Respondents == null)
                    courtCase.Respondents = new List<Participant>();
                courtCase.Respondents.Add(new Participant()
                {
                    Individual = model.ToIndividual(manager),
                });

            }
            return Redirect("/Court/NewRespondent");
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddJuridicalRespondent(JuridicialModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("NewRespondent", model);
            }
            else
            {
                CourtCase courtCase = Session["court-case"] as CourtCase;
                if (courtCase.Respondents == null)
                    courtCase.Respondents = new List<Participant>();
                courtCase.Respondents.Add(new Participant()
                {
                    Juridical = model.ToJuridical(manager),
                });

            }
            return Redirect("/Court/NewRespondent");
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddPhysicalRespondent(PhysicalModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("NewRespondent", model);
            }
            else
            {
                CourtCase courtCase = Session["court-case"] as CourtCase;
                if (courtCase.Respondents == null)
                    courtCase.Respondents = new List<Participant>();
                courtCase.Respondents.Add(new Participant()
                {
                    Physical = model.ToPhysical(manager),
                });

            }
            return Redirect("/Court/NewRespondent");

        }

        [Authorize]
        public ActionResult NewClaim()
        {
            CourtCase courtCase = Session["court-case"] as CourtCase;
            if (courtCase == null || courtCase.Claimant == null || courtCase.Respondents == null || courtCase.Respondents.Count == 0)
                return Redirect("/Court/Index");
            else
                return View();
        }

        [Authorize]
        public ActionResult SaveClaim(ClaimModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("NewClaim", model);
            }
            else
            {
                CourtCase courtCase = Session["court-case"] as CourtCase;
                courtCase.Claim = model.ToClaim(manager);
                courtCase.CourtUser = CurrentUser;
                courtCase.SubmissionDate = DateTime.Now;
                courtCase = manager.AddCourtCase(courtCase);
                currentUser = null;
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        public ActionResult Resolve(int id)
        {
            if (CurrentUser.Level == AccessLevel.User)
                return View("ViewCase", new ForwardModel()
                {
                    CaseId = id
                });
            else
            {
                CourtCase cCase = manager.GetCase(id);
                return View("ForwardCase", new ForwardModel()
                {
                    CaseId = id,
                    ActionId = (int)cCase.Decision,
                    Comments = cCase.Commentary,
                    Date = cCase.GatheringDate,
                    AttachmentName = cCase.AttachmentName
                });
            }
        }
        [Authorize]
        [HttpPost]
        public ActionResult Resolve(ForwardModel model)
        {
            if (CurrentUser.Level == AccessLevel.User)
                return Redirect("/Court/Index");
            else
            {
                if (model.ActionId == 2 && String.IsNullOrEmpty(model.Comments))
                {
                    ModelState.AddModelError("", "При отклонении иска комментарий обязателен");
                    return View("ForwardCase", model);
                }
                if (model.ActionId == 1 && !model.Date.HasValue)
                {
                    ModelState.AddModelError("", "При принятии иска дата заседания должна быть указана");
                    return View("ForwardCase", model);
                }
                if (model.ActionId == 2 && model.Date.HasValue)
                {
                    ModelState.AddModelError("", "При отклонении иска дата заседания должна быть пустая");
                    return View("ForwardCase", model);
                }
                CourtCase courtCase = manager.GetCase(model.CaseId);
                byte[] data = null;
                string fileName = null;
                if (model.Attachment != null && model.Attachment.ContentLength > 0)
                {
                    using (BinaryReader binaryReader = new BinaryReader(model.Attachment.InputStream))
                    {
                        data = binaryReader.ReadBytes(model.Attachment.ContentLength);
                        fileName = model.Attachment.FileName;
                    }
                }
                manager.UpdateCase(model.CaseId, (Decision)model.ActionId, model.Comments, model.Date, data, fileName);
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        public ActionResult CountryList()
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                return View(manager.GettAllAvailableCountries());
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        public ActionResult DeleteCountry(int id)
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                manager.DeleteCountry(id);
                return Redirect("/Court/CountryList");
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditCountry(Country model)
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                manager.EditCountry(model.Id, model.Name);
                return Redirect("/Court/CountryList");
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        public ActionResult EditCountry(int id)
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                Country country = manager.GetCountry(id);
                if (country == null)
                {
                    country = new Country()
                    {
                        Id = id
                    };
                }
                return View(country);
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        public ActionResult CollegiateList()
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                return View(manager.GetAllCollegiates());
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        public ActionResult DeleteCollegiate(int id)
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                manager.DeleteCollegiate(id);
                return Redirect("/Court/CollegiateList");
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditCollegiate(Collegiate model)
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                manager.EditCollegiate(model.Id, model.Name);
                return Redirect("/Court/CollegiateList");
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        public ActionResult EditCollegiate(int id)
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                Collegiate collegiate = manager.GetCollegiate(id);
                if (collegiate == null)
                {
                    collegiate = new Collegiate()
                    {
                        Id = id
                    };
                }
                return View(collegiate);
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        public ActionResult ReasonList()
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                return View(manager.GetAllClaimReasons());
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        public ActionResult DeleteReason(int id)
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                manager.DeleteReason(id);
                return Redirect("/Court/ReasonList");
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditReason(ClaimReason model)
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                manager.EditReason(model.Id, model.Name);
                return Redirect("/Court/ReasonList");
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }

        [Authorize]
        public ActionResult Download(int id)
        {
            CourtCase court = manager.GetCase(id);
            return File(court.Attachment, System.Net.Mime.MediaTypeNames.Application.Octet, court.AttachmentName);
        }

        [Authorize]
        public ActionResult Delete(int id)
        {
            manager.DeleteAttachment(id);
            return Redirect("/Court/Resolve?id=" + id);
        }

        [Authorize]
        public ActionResult EditReason(int id)
        {
            if (CurrentUser.Level == AccessLevel.Administrator)
            {
                ClaimReason reason = manager.GetReason(id);
                if (reason == null)
                {
                    reason = new ClaimReason()
                    {
                        Id = id
                    };
                }
                return View(reason);
            }
            else
            {
                return Redirect("/Court/Index");
            }
        }
    }
}